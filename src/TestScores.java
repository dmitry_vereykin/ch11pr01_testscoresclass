/**
 * Created by Dmitry Vereykin on 7/23/2015.
 */
import java.text.DecimalFormat;

public class TestScores {
    private double[] scoreArray;

    public TestScores(double[] arr) throws IllegalArgumentException {
        scoreArray = new double[arr.length];

        for (int i = 0; i < arr.length; i++) scoreArray[i] = arr[i];

        DecimalFormat niceDouble = new DecimalFormat("#.##");
        System.out.print("The Avg: " + niceDouble.format(getAverage()));
    }

    public double getAverage() throws IllegalArgumentException {
        double total = 0;
        for (int i = 0; i < scoreArray.length; i++) {
            if(scoreArray[i] < 0 || scoreArray[i] > 100)
                throw new IllegalArgumentException("Score #"+ ( i + 1) + " is out of range.");
            total += scoreArray[i];
        }
        return total / scoreArray.length;
    }

}


